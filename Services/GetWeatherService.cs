﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WeatherApp.Models;
using System.Net;

namespace WeatherApp.Services
{
    public class WeatherService : IWeatherService
    {
        private const string _baseUrl = "http://api.openweathermap.org/data/2.5/weather?q=";
        private const string _apiKey = "c85f068eccd640741b40c22a01cb747f";
        private readonly HttpClient _client;

        public WeatherService(HttpClient client)
        {
            _client = client;
        }

        public async Task<WeatherInfo> GetWeather(string city)
        {
            var uri = $"{_baseUrl}{city}&appid={_apiKey}&units=metric";

            var httpResponse = await _client.GetAsync(uri);

            if (httpResponse.StatusCode == HttpStatusCode.NotFound)
            {
                throw new Exception("Une erreur s'est produite : Cette ville n'a pas été trouvé");
            }

            if (!httpResponse.IsSuccessStatusCode)
            {
                throw new Exception("Une erreur s'est produite. Ne peut pas terminer la tâche");
            }

            var content = await httpResponse.Content.ReadAsStringAsync();

            var weatherInfo = JsonConvert.DeserializeObject<WeatherInfo>(content);

            return weatherInfo;
        }
    }
}