﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherApp.Models
{
    public class WeatherInfoViewModel
    {
        [Required(ErrorMessage = "Veuillez saisir la ville")]
        [Display(Name = "Ville")]
        public string City { get; set; }

        public WeatherInfo WeatherInfo { get; set; }
    }
}