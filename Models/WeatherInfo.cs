﻿namespace WeatherApp.Models
{
    public class WeatherInfo
    {
        public Main Main { get; set; }

        public WeatherInfo()
        {
            Main = new Main();
        }
    }
}