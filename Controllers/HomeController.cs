﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using WeatherApp.Models;
using WeatherApp.Services;

namespace WeatherApp.Controllers
{
    public class HomeController : Controller
    {
        private IWeatherService _weatherService;

        public HomeController(IWeatherService weatherService)
        {
            _weatherService = weatherService;
        }

        public IActionResult Index()
        {
            var vm = new WeatherInfoViewModel();

            vm.WeatherInfo = new WeatherInfo();

            return View(vm);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public IActionResult GetWeatherInfo(WeatherInfoViewModel vm)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    vm.WeatherInfo = _weatherService.GetWeather(vm.City).Result;

                    return View("Index", vm);
                }
                catch (Exception e)
                {
                    TempData["ErrorMessage"] = e.InnerException.Message;

                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Index");
        }
    }
}